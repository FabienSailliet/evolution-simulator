
import pr3_5.evolution_simulator.render.RenderWindow;
import pr3_5.evolution_simulator.simulator.Organism;
import pr3_5.evolution_simulator.simulator.OrganismSimulation;
import pr3_5.evolution_simulator.simulator.Simulator;
public class mainTest
{

	public static void main(String[] args) 
	{
//		System.out.println(g.toString());
//		System.out.println(g2.toString());
//		System.out.println(g3.toString());
		
		Organism o = new Organism();
		System.out.println(o.toString());
		
		o.addLimb(2f, 0,true,20);
		o.addLimb(2f, 1,false,15);
		System.out.println(o.toString());
		
		Simulator s = new Simulator(9.81f,2,4,0,0.5f,o);
		System.out.println(s.toString());
		
		s.newSimulation();
		System.out.println(s.toString());
		
		OrganismSimulation os = new OrganismSimulation(s.getBaseOrganism(),s);
		
		
		
//		Organism o2 = new Organism();
//		System.out.println(o2.toString());
//		
//		o2.addLimb(20, 0,true);
//		o2.addLimb(20, 1,false);System.out.println(o2.toString());
//		
//		Organism o3 = o.reproduce(o2);
//		System.out.println(o3.toString());
		
//		EvolutionSimulatorInterface.launch(EvolutionSimulatorInterface.class,args);
		
		
	}

}
