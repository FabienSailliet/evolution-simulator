package pr3_5.evolution_simulator.display;

public class Utilities {
	/**
	 * Return coordinates, transformed into pixel
	 * 
	 * @param x
	 * @param scale
	 * @param offset
	 * @param screenSize
	 * @return
	 */
	public static int toPixCoord(double x, int scale, double offset, int screenSize) {
		return (int) ((x - offset) * scale + (screenSize / 2));
	}
	
	/**
	 * Return size, transformed into pixels
	 * 
	 * @param x
	 * @param scale
	 * @return
	 */
	public static int toPixSize(double x, int scale) {
		return Utilities.toPixCoord(x, scale, 0, 0);
	}
}
