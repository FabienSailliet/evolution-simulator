package pr3_5.evolution_simulator.display;
/**
 * Set of value to determine the spacing between each elements on the UI according to the screen resolution.
 * @author Morel Jérémy
 *
 */
public enum ToSpacing {

	EDIT_BUTTON_TOP_SPACING ("100.0"),
	EDIT_BUTTON_LEFT_SPACING("50.0"),
	EDIT_BTN_BOX_SPACING("10.0"),
	VALIDATE_RIGHT_SPACING("100.0"),
	VALIDATE_BOTTOM_SPACING("100.0"),
	DISPLAY_BUTTON_SPACING("10.0"),
	DISPLAY_BUTTON_BOTTOM_SPACING("100.0"),
	DISPLAY_BUTTON_RIGHT_SPACING("400.0"),
	SIMULATION_CONFIG_SLIDE_SPACING("100.0"),
	SLIDE_RIGHT_SPACING("350.0"),
	SLIDE_TOP_SPACING("80.0"),
	MUTACHANCE_LABEL_RIGHT_SPACING("1150.0"),
	GRAVITY_LABEL_RIGHT_SPACING("900.0"),
	NB_GEN_LABEL_RIGHT_SPACING("650.0"),
	FLOOR_ANGLE_RIGHT_SPACING("400.0"),
	CONFIG_LABEL_TOP_SPACING("20.0"),
	TEXTFIELD_SPACING("100.0"),
	TEXTFIELD_RIGHT_SPACING("350.0"),
	TEXTFIELD_TOP_SPACING("100.0"),
	SIMUPROGRESS_BOTTOM_SPACING("100.0"),
	TIMELEFT_BOTTOM_SPACING("110.0"),
	TIMELEFT_LEFT_SPACING("385.0"),
	SIMUPROGRESS_LEFT_SPACING("20");
	
	/**
	 * Spacing between elements and the border of the window.
	 */
	private String spacing;

	/**
	 * ToSpacing constructor.
	 * @param spacing
	 */
	ToSpacing(String spacing)
	{
		this.spacing = spacing;
	}
	
	/**
	 * Determination of spacing according to the screen resolution
	 * @param initSpacing : initial spacing for 1920x1080 resolution.
	 * @param screenWidth : Width of the screen
	 * @param maxScreenWidth : Width of the max screen width (1920px)
	 * @return the result of the calculation
	 */
	public static double calcSpacing(String initSpacing, double screenWidth, double maxScreenWidth)
	{
		return Double.valueOf(initSpacing)*(screenWidth/maxScreenWidth);
	}
	
	/**
	 * @return the spacing for a 1920x1080 resolution.
	 */
	public String toString()
	{
		return this.spacing;
	}
}
