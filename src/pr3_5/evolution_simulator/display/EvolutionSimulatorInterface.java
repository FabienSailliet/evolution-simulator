package pr3_5.evolution_simulator.display;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Optional;

import javafx.application.Application;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.geometry.Rectangle2D;
import javafx.scene.Scene;
import javafx.scene.control.Alert;
import javafx.scene.control.Alert.AlertType;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.ProgressBar;
import javafx.scene.control.Slider;
import javafx.scene.control.TextField;
import javafx.scene.control.TextInputDialog;
import javafx.scene.control.ToggleButton;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import javafx.scene.paint.Color;
import javafx.scene.shape.Circle;
import javafx.scene.shape.Line;
import javafx.scene.shape.Shape;
import javafx.stage.Screen;
import javafx.stage.Stage;
import pr3_5.evolution_simulator.render.RenderWindow;
import pr3_5.evolution_simulator.simulator.Limb;
import pr3_5.evolution_simulator.simulator.Organism;
import pr3_5.evolution_simulator.simulator.Simulator;
/**
 * To build the editor interface.
 * @author Morel Jérémy
 *
 */
public class EvolutionSimulatorInterface extends Application {

	private static final double MAX_SCREEN_WIDTH = 1920;
	private static final double MAX_SCREEN_HEIGHT = 1080;
	@Override
	public void start(Stage primaryStage) throws Exception {
		
		
		//progressBar
		
		ProgressBar simuProgress = new ProgressBar();
		simuProgress.setProgress(0f);
		
		LinkedList<LinkedList<Object>> parents = new LinkedList<LinkedList<Object>>();
		HashMap<Object,Object> ChildParent = new HashMap<Object,Object>();
		
		primaryStage.setMaximized(true);
		primaryStage.setResizable(false);
		primaryStage.setFullScreen(false);
		
		/**
		 * main layout
		 */
		AnchorPane root = new AnchorPane();
		
		/**
		 * Current screen width.
		 */
		double screenWidth = Screen.getPrimary().getVisualBounds().getWidth();
		
		/**
		 * Current screen height.
		 */
		double screenHeight = Screen.getPrimary().getVisualBounds().getHeight();
		
		/**
		 * Screen properties.
		 */
		Rectangle2D primaryScreenBounds = Screen.getPrimary().getVisualBounds();
		
		primaryStage.setMaxWidth(screenWidth);
		primaryStage.setMinWidth(screenWidth);
		primaryStage.setMaxHeight(screenHeight);
		primaryStage.setMinHeight(screenHeight);
		
		/* Editor Button initialization */
		
		/**
		 * to Validate the create organism and launch the simulation
		 */
		Button validateBtn = new Button("Simulate");
		validateBtn.setPrefSize(ToResize.resizing(ToResize.BOTTOM_BTN_WIDTH.toString(), screenWidth, MAX_SCREEN_WIDTH), ToResize.resizing(ToResize.BOTTOM_BTN_HEIGHT.toString(), screenHeight, MAX_SCREEN_HEIGHT));
		
		/**
		 * to delete Limb on layout
		 */
		final ToggleButton rubberBtn = new ToggleButton("Rubber");
		rubberBtn.setPrefSize(ToResize.resizing(ToResize.MAX_EDIT_BTN_WIDTH.toString(), screenWidth, MAX_SCREEN_WIDTH), ToResize.resizing(ToResize.MAX_EDIT_BTN_HEIGHT.toString(), screenHeight, MAX_SCREEN_HEIGHT));

		/**
		 * To create Limb on editor
		 */
		final ToggleButton limbBtn = new ToggleButton("Limb");
		limbBtn.setPrefSize(ToResize.resizing(ToResize.MAX_EDIT_BTN_WIDTH.toString(), screenWidth, MAX_SCREEN_WIDTH), ToResize.resizing(ToResize.MAX_EDIT_BTN_HEIGHT.toString(), screenHeight, MAX_SCREEN_HEIGHT));
		
		
		/**
		 * To fix articulation between limbs
		 */
		final ToggleButton fixBtn = new ToggleButton("Fix");
		fixBtn.setPrefSize(ToResize.resizing(ToResize.MAX_EDIT_BTN_WIDTH.toString(), screenWidth, MAX_SCREEN_WIDTH), ToResize.resizing(ToResize.MAX_EDIT_BTN_HEIGHT.toString(), screenHeight, MAX_SCREEN_HEIGHT));
		
		/**
		 * Set the editor button oriented vertically
		 */
		VBox editBtn = new VBox(rubberBtn,limbBtn,fixBtn);
		editBtn.setSpacing(ToSpacing.calcSpacing(ToSpacing.EDIT_BTN_BOX_SPACING.toString(), screenWidth, MAX_SCREEN_WIDTH));
		
		/* Set editor buttons on the layout */
		
		AnchorPane.setTopAnchor(editBtn, ToSpacing.calcSpacing(ToSpacing.EDIT_BUTTON_TOP_SPACING.toString(),screenWidth, MAX_SCREEN_WIDTH));
		AnchorPane.setLeftAnchor(editBtn, ToSpacing.calcSpacing(ToSpacing.EDIT_BUTTON_LEFT_SPACING.toString(), screenWidth, MAX_SCREEN_WIDTH));
		AnchorPane.setBottomAnchor(validateBtn, ToSpacing.calcSpacing(ToSpacing.VALIDATE_BOTTOM_SPACING.toString(), screenWidth, MAX_SCREEN_WIDTH));
		AnchorPane.setRightAnchor(validateBtn, ToSpacing.calcSpacing(ToSpacing.VALIDATE_RIGHT_SPACING.toString(), screenWidth, MAX_SCREEN_WIDTH));
		
		AnchorPane.setBottomAnchor(simuProgress, ToSpacing.calcSpacing(ToSpacing.SIMUPROGRESS_BOTTOM_SPACING.toString(), screenWidth, MAX_SCREEN_WIDTH));
		AnchorPane.setLeftAnchor(simuProgress, ToSpacing.calcSpacing(ToSpacing.SIMUPROGRESS_LEFT_SPACING.toString(), screenWidth, MAX_SCREEN_WIDTH));
		
		
		/* Initialization of Sliders to configure the simulation */
		
		/**
		 * To set mutation chance of a genom
		 */
		Slider mutationChance = new Slider(0.0,1.0,0.5);
		
		/**
		 * To set the gravity the world simulation
		 */
		Slider gravity = new Slider(0.0,10.0,9.81);
		
		/**
		 * To choose the number of generation
		 */
		Slider nbOfGen = new Slider(0.0,5000.0,50);
		
		/**
		 * to choose the angle of the floor
		 */
		Slider floorAngle = new Slider(0.0,90.0,0.0);
		mutationChance.setPrefWidth(ToResize.resizing(ToResize.SLIDER_WIDTH.toString(), screenWidth, MAX_SCREEN_WIDTH));
		gravity.setPrefWidth(ToResize.resizing(ToResize.SLIDER_WIDTH.toString(), screenWidth, MAX_SCREEN_WIDTH));
		nbOfGen.setPrefWidth(ToResize.resizing(ToResize.SLIDER_WIDTH.toString(), screenWidth, MAX_SCREEN_WIDTH));
		floorAngle.setPrefWidth(ToResize.resizing(ToResize.SLIDER_WIDTH.toString(), screenWidth, MAX_SCREEN_WIDTH));
		
		/**
		 * Set slider horizontally oriented
		 */
		HBox simulationConfig = new HBox(mutationChance,gravity,nbOfGen,floorAngle);
		simulationConfig.setSpacing(ToSpacing.calcSpacing(ToSpacing.SIMULATION_CONFIG_SLIDE_SPACING.toString(), screenWidth, MAX_SCREEN_WIDTH));
		
		
		/* Set Sliders on layout */
		
		AnchorPane.setRightAnchor(simulationConfig, ToSpacing.calcSpacing(ToSpacing.SLIDE_RIGHT_SPACING.toString(), screenWidth, MAX_SCREEN_WIDTH));
		AnchorPane.setTopAnchor(simulationConfig, ToSpacing.calcSpacing(ToSpacing.SLIDE_TOP_SPACING.toString(), screenWidth, MAX_SCREEN_WIDTH));
	
		/* Initialization of Label on layout */
		
		/**
		 * mutation chance label
		 */
		Label mutaChance = new Label("Mutation\n Chance");
		
		/**
		 * gravity label
		 */
		Label gravityConfig = new Label("Gravity");
		
		/**
		 * number of Generation label
		 */
		Label numberOfGeneration = new Label("Number of\n Generation");
		
		/**
		 * floor angle label
		 */
		Label floorAngleConfig = new Label("Floor Angle");
		
		
		/* Set label on layout (RIGHT) */
		AnchorPane.setRightAnchor(mutaChance, ToSpacing.calcSpacing(ToSpacing.MUTACHANCE_LABEL_RIGHT_SPACING.toString(), screenWidth, MAX_SCREEN_WIDTH));
		AnchorPane.setRightAnchor(gravityConfig, ToSpacing.calcSpacing(ToSpacing.GRAVITY_LABEL_RIGHT_SPACING.toString(), screenWidth, MAX_SCREEN_WIDTH));
		AnchorPane.setRightAnchor(numberOfGeneration, ToSpacing.calcSpacing(ToSpacing.NB_GEN_LABEL_RIGHT_SPACING.toString(), screenWidth, MAX_SCREEN_WIDTH));
		AnchorPane.setRightAnchor(floorAngleConfig, ToSpacing.calcSpacing(ToSpacing.FLOOR_ANGLE_RIGHT_SPACING.toString(), screenWidth, MAX_SCREEN_WIDTH));
		
		/* Set label on layout (TOP) */
		
		AnchorPane.setTopAnchor(mutaChance, ToSpacing.calcSpacing(ToSpacing.CONFIG_LABEL_TOP_SPACING.toString(), screenWidth, MAX_SCREEN_WIDTH));
		AnchorPane.setTopAnchor(gravityConfig, ToSpacing.calcSpacing(ToSpacing.CONFIG_LABEL_TOP_SPACING.toString(), screenWidth, MAX_SCREEN_WIDTH));
		AnchorPane.setTopAnchor(numberOfGeneration, ToSpacing.calcSpacing(ToSpacing.CONFIG_LABEL_TOP_SPACING.toString(), screenWidth, MAX_SCREEN_WIDTH));
		AnchorPane.setTopAnchor(floorAngleConfig, ToSpacing.calcSpacing(ToSpacing.CONFIG_LABEL_TOP_SPACING.toString(), screenWidth, MAX_SCREEN_WIDTH));
		
		simuProgress.setPrefWidth(ToResize.resizing(ToResize.SIMUPROGRESS_WIDTH.toString(), screenWidth, MAX_SCREEN_WIDTH));
		
		/* Initialization of TextField */
		
		/**
		 * mutation chance text field (not editable)
		 */
		TextField mutaField = new TextField();
		
		/**
		 * gravity text field (not editable)
		 */
		TextField gravityField = new TextField();
		
		/**
		 * number of generation text field (not editable)
		 */
		TextField nbGenField = new TextField();
		
		/**
		 * floor angle text field
		 */
		TextField floorField = new TextField();
		mutaField.setPrefSize(ToResize.resizing(ToResize.TEXTFIELD_WIDTH.toString(), screenWidth, MAX_SCREEN_WIDTH), ToResize.resizing(ToResize.TEXTFIELD_HEIGHT.toString(), screenHeight, MAX_SCREEN_HEIGHT));
		gravityField.setPrefSize(ToResize.resizing(ToResize.TEXTFIELD_WIDTH.toString(), screenWidth, MAX_SCREEN_WIDTH), ToResize.resizing(ToResize.TEXTFIELD_HEIGHT.toString(), screenHeight, MAX_SCREEN_HEIGHT));
		nbGenField.setPrefSize(ToResize.resizing(ToResize.TEXTFIELD_WIDTH.toString(), screenWidth, MAX_SCREEN_WIDTH), ToResize.resizing(ToResize.TEXTFIELD_HEIGHT.toString(), screenHeight, MAX_SCREEN_HEIGHT));
		floorField.setPrefSize(ToResize.resizing(ToResize.TEXTFIELD_WIDTH.toString(), screenWidth, MAX_SCREEN_WIDTH), ToResize.resizing(ToResize.TEXTFIELD_HEIGHT.toString(), screenHeight, MAX_SCREEN_HEIGHT));
		mutaField.setEditable(false);
		gravityField.setEditable(false);
		nbGenField.setEditable(false);
		floorField.setEditable(false);
		
		//Time left text filed
		
		TextField TimeLeft = new TextField();
		
		TimeLeft.setPrefSize(ToResize.resizing(ToResize.TIMELEFT_WIDTH.toString(), screenWidth, MAX_SCREEN_WIDTH), ToResize.resizing(ToResize.TIMELEFT_HEIGHT.toString(), screenHeight, MAX_SCREEN_HEIGHT));
		AnchorPane.setBottomAnchor(TimeLeft, ToSpacing.calcSpacing(ToSpacing.TIMELEFT_BOTTOM_SPACING.toString(), screenWidth, MAX_SCREEN_WIDTH));
		AnchorPane.setLeftAnchor(TimeLeft, ToSpacing.calcSpacing(ToSpacing.TIMELEFT_LEFT_SPACING.toString(), screenWidth, MAX_SCREEN_WIDTH));
		
		/**
		 * Set label horizontally oriented
		 */
		HBox textField = new HBox(mutaField,gravityField,nbGenField,floorField);
		textField.setSpacing(ToSpacing.calcSpacing(ToSpacing.TEXTFIELD_SPACING.toString(), screenWidth, MAX_SCREEN_WIDTH));
		
		/* Set TextField on layout */
		
		AnchorPane.setRightAnchor(textField, ToSpacing.calcSpacing(ToSpacing.TEXTFIELD_RIGHT_SPACING.toString(), screenWidth, MAX_SCREEN_WIDTH));
		AnchorPane.setTopAnchor(textField, ToSpacing.calcSpacing(ToSpacing.TEXTFIELD_TOP_SPACING.toString(), screenWidth, MAX_SCREEN_WIDTH));
		
		/* Binding initial value */
		
		mutaField.textProperty().bind(mutationChance.valueProperty().asString("%.2f"));
		nbGenField.textProperty().bind(nbOfGen.valueProperty().asString("%.0f"));
		gravityField.textProperty().bind(gravity.valueProperty().asString("%.2f"));
		floorField.textProperty().bind(floorAngle.valueProperty().asString("%.2f"));
		
		/**
		 * Binding value of the simulation configuration
		 */
		
		mutationChance.valueProperty().addListener(new ChangeListener<Number>(){
			
			public void changed(ObservableValue<? extends Number> arg0, Number oldValue, Number newValue)
			{
				mutaField.textProperty().bind(mutationChance.valueProperty().asString("%.2f"));
				//TODO mutChance = mutationChance.valueProperty()
			}
		});
				
		gravity.valueProperty().addListener(new ChangeListener<Number>() {
			
			public void changed(ObservableValue<? extends Number> arg0, Number oldValue, Number newValue)
			{
				gravityField.textProperty().bind(gravity.valueProperty().asString("%.2f"));
			}
		});
		
		nbOfGen.valueProperty().addListener(new ChangeListener<Number>(){
			
			public void changed(ObservableValue<? extends Number> arg0, Number oldValue, Number newValue)
			{
				nbGenField.textProperty().bind(nbOfGen.valueProperty().asString("%.0f"));
			}
		});
		
		floorAngle.valueProperty().addListener(new ChangeListener<Number>(){
			
			public void changed(ObservableValue<? extends Number> arg0, Number oldValue, Number newValue)
			{
				floorField.textProperty().bind(floorAngle.valueProperty().asString("%.2f"));
			}
		});
		
		
		/* Set the first organism on layout */
		
		LinkedList<Object> startOrga = new LinkedList<Object>();
		Organism o = new Organism();
		float n = o.getLimbLength(0);
		int length = (int)n*((int)(screenWidth/50)/2);
		Line l = new Line();
		l.setStartY(screenHeight/2);
		l.setStartX(screenWidth/2-length);
		l.setEndX(screenWidth/2+length);
		l.setEndY(screenHeight/2);
		l.setStrokeWidth(6.0);
		Circle c1 = new Circle();
		Circle c2 = new Circle();
		c1.setCenterX(screenWidth/2-length);
		c1.setCenterY(screenHeight/2);
		c1.setRadius(6.0);
		c1.setFill(Color.BLUE);
		c2.setCenterX(screenWidth/2+length);
		c2.setCenterY(screenHeight/2);
		c2.setRadius(6.0);
		c2.setFill(Color.BLUE);
		root.getChildren().addAll(l,c1,c2);
		startOrga.add(l);
		startOrga.add(c1);
		startOrga.add(c2);
		parents.add(new LinkedList<Object>());
		parents.get(0).add(l);
		parents.get(0).add(c1);
		parents.get(0).add(c2);
		
		 /* Set element on layout */
		
		root.getChildren().add(textField);
		root.getChildren().addAll(mutaChance,gravityConfig,numberOfGeneration,floorAngleConfig);
		root.getChildren().add(simulationConfig);
		root.getChildren().add(editBtn);
		root.getChildren().add(validateBtn);
		root.getChildren().add(simuProgress);
		root.getChildren().add(TimeLeft);
		
		
		
		/* This will allow us to show elements on layout */
		
	    Scene scene = new Scene(root,primaryScreenBounds.getWidth(),primaryScreenBounds.getHeight());
	    
	    /* Detect click on layout */
	    
	    
	    /**
	     * set Action on the rubber button
	     */
	 
	    rubberBtn.setOnAction((ActionEvent e) -> {
	        if(rubberBtn.isSelected())
	        {
	        	if(limbBtn.isSelected() || fixBtn.isSelected())
	        	{
	        		limbBtn.setSelected(false);
	        		fixBtn.setSelected(false);
	        	}
	        	root.setOnMouseClicked(new EventHandler<MouseEvent>() {
	        		
	        		public void handle(MouseEvent event)
	        		{
	        			
	        			Object target = event.getTarget();
	        			if(parents.get(0).contains(target))
	        			{
	        				return;
	        			}
	        			if(target instanceof Shape)
	        			{
	        				deleteOnInterface(parents, ChildParent, root, target, o);
	        			}
	        		}
	        	});
	        }
	    });
	    
	    
	    //set action on validate button
	    validateBtn.setOnAction((ActionEvent e) -> {
		    											double muc = mutationChance.getValue();
													    double nbs = nbOfGen.getValue();
													    double ang = floorAngle.getValue();
													    double g = gravity.getValue();
													    this.validate(TimeLeft,simuProgress,parents,ChildParent,screenWidth,screenHeight,muc,g,nbs,ang);
												    });

	    /**
	     * Set action on limb button
	     */
	    Object[] p = new Object[1];
	    Object[] preGuideLine = new Object[1];
	    
	    limbBtn.setOnAction((ActionEvent e) -> {
	    	
	    	if(limbBtn.isSelected())
	        {
	        	if(rubberBtn.isSelected() || fixBtn.isSelected())
	        	{
	        		rubberBtn.setSelected(false);
	        		fixBtn.setSelected(false);
	        	}
	        	
	        	List<Coordinate> lCoord = new LinkedList<Coordinate>();
	        	root.setOnMouseClicked(new EventHandler<MouseEvent>() {
	        		
		        	public void handle(MouseEvent event)
		        	{
		        			Object s1 = event.getTarget();
		        			if(lCoord.size() == 0)
		        			{
		        				if(s1 instanceof Circle)
				        		{
				        			lCoord.add(new Coordinate(event.getSceneX(),event.getSceneY()));
				        			p[0] = s1;
				        		}
				        		
				        		else
				        		{
				        			Alert alert = new Alert(AlertType.WARNING);
				        			alert.setHeaderText("InvalidPosException");
			        				alert.setContentText("Please, click on circle");
			        				alert.showAndWait();
			        				return;
				        		}
		        			}
		        			else
		        			{
		        				if(s1 instanceof AnchorPane)
				        		{
				        			lCoord.add(new Coordinate(event.getSceneX(),event.getSceneY()));
				        		}
				        		else 
				        		{
				        			Alert alert = new Alert(AlertType.WARNING);
				        			alert.setHeaderText("InvalidPosException");
				        			alert.setContentText("Please, click on layout");
				        			alert.showAndWait();
				        			return;
				        		}
		        			}
			        		
		        		if(lCoord.size() == 2)
		        		{
		        			Line l = new Line();
	        				Circle fCircle = new Circle();
	        				Coordinate[] line = new Coordinate[lCoord.size()];
	        				line = lCoord.toArray(line);
	        				
	        				l.setStrokeWidth(4.0);
	        				
	        				fCircle.setCenterX(line[1].getX());
	        				fCircle.setCenterY(line[1].getY());
	        				fCircle.setRadius(6.0);
	        				fCircle.setFill(Color.BLUE);
	        				
	        				
	        				
		        		
        				
		        			if(parents.get(0).get(1) == p[0])
		        			{
		        				l.setEndX(line[0].getX());
		        				l.setEndY(line[0].getY());
		        				l.setStartX(line[1].getX());
		        				l.setStartY(line[1].getY());
		        				ChildParent.put(parents.get(0).get(0),p[0]);
		        				parents.addFirst(new LinkedList<Object>());
		        				parents.getFirst().add(l);
		        				parents.getFirst().add(fCircle);
		        				parents.getFirst().add(p[0]);
		        				parents.get(1).remove(1);
		        			}
		        			else
		        			{
		        				l.setStartX(line[0].getX());
		        				l.setStartY(line[0].getY());
		        				l.setEndX(line[1].getX());
		        				l.setEndY(line[1].getY());
		        				ChildParent.put(l, p[0]);
		        				parents.add(new LinkedList<Object>());
		        				parents.getLast().add(l);
		        				parents.getLast().add(fCircle);
		        			}
		        			
		        			root.getChildren().add(0, l);
	        				root.getChildren().add(fCircle);
	        				root.getChildren().remove(preGuideLine[0]);
	        				preGuideLine[0]=null;
		        			lCoord.clear();
		        		}
        				
        				
        				
        				
        				
		        			
		        	}
	        	});
	        	root.setOnMouseMoved(new EventHandler<MouseEvent>() 
	        	{

					@Override
					public void handle(MouseEvent event) 
					{
						if(lCoord.size() == 1)
						{
							Line newL = new Line();
							newL.setStartX(lCoord.get(0).getX());
							newL.setStartY(lCoord.get(0).getY());
							newL.setEndX(event.getX() +(event.getX() < newL.getStartX()?+2:-2));
							newL.setEndY(event.getY() +(event.getY() < newL.getStartY()?+2:-2));
							newL.setStrokeWidth(4.0);
							newL.setFill(Color.BLUE);
							root.getChildren().addAll(newL);
							if(preGuideLine[0] != null)
							{
								root.getChildren().remove(preGuideLine[0]);
							}
							preGuideLine[0] = newL;
						}
					}
				});
	        }
	    });
	    
	    /**
	     * Set action on fix Button
	     */
	    
	    fixBtn.setOnAction((ActionEvent e) -> {
	    	
	    	if(fixBtn.isSelected())
	        {
	        	if(limbBtn.isSelected() || rubberBtn.isSelected())
	        	{
	        		rubberBtn.setSelected(false);
	        		limbBtn.setSelected(false);
	        	}
	        	root.setOnMouseClicked(new EventHandler<MouseEvent>(){

					public void handle(MouseEvent event) {
						
						Object s = event.getTarget();	
						if(s instanceof Circle)
						{
							if(ChildParent.containsValue(s))
							{
								Circle shape = (Circle)s;
								shape.setFill(shape.getFill() == Color.RED ? Color.BLUE:Color.RED);
							}
						}
						
					}
	        		
	        	});
	        }
	    });
	    
	    /* Display elements */
	    
		primaryStage.setScene(scene); 
		primaryStage.show(); 
		
	}
	
	
	private void validate(TextField TimeLeft,ProgressBar b,LinkedList<LinkedList<Object>> parents,HashMap<Object,Object> cP,double screenWidth,double screenHeight,double muc,double g,double nbs,double ang) 
	{
		TimeLeft.setText("");
		Thread window = new Thread()
		{
			public void run()
			{
				Organism o = EvolutionSimulatorInterface.generateOrganism(parents, cP, screenWidth, screenHeight);
				//System.out.println(muc + " "+ g +"  "+(int)nbs+" "+ang);
				Simulator s = new Simulator((float)g,(int)nbs,10,(int)ang,(float)muc,o);
				
				s.run(TimeLeft,b);
				//System.out.println("o = " + s.getOrga());
				new RenderWindow(s.getOrgaSimu(0),20,"Generation 1");
				new RenderWindow(s.getOrgaSimu(1),20,"Generation "+(int)nbs);
			}
		};
		window.start();
				
				
//		OrganismSimulation os = new OrganismSimulation(s.getBaseOrganism(),s);
//		
//		RenderWindow sw = new RenderWindow(os,20);
	}

	private void deleteOnInterface(LinkedList<LinkedList<Object>> parents,HashMap<Object,Object> cP,AnchorPane root,Object target,Organism o)
	{
		for(int i=0;i<parents.size();i++)
		{
			if(parents.get(i).contains(target))
			{
				for(int j=0;j<parents.get(i).size();j++)
				{
					Object t = parents.get(i).get(j);
					if(t instanceof Circle)
					{
						Object[] keys = cP.keySet().toArray();
						for(int k=0;k<keys.length;k++)
						{
							Object val = cP.get(keys[k]);
							if(val == t)
							{
								deleteOnInterface(parents,cP,root,keys[k],o);
							}
						}
					}
					root.getChildren().remove(parents.get(i).get(j));
					
					parents.get(i).remove(j);
					j--;
				}
				parents.remove(i);
				i--;
			}
		}
	}
	private static Organism generateOrganism(LinkedList<LinkedList<Object>> parents,HashMap<Object,Object> cP,double screenWidth,double screenHeight)
	{
		Circle c1 = (Circle)parents.get(0).get(1);
		Circle c2 = (Circle)parents.get(0).get(2);
		float rootLength = (float)Math.sqrt(Math.pow(c1.getCenterX() - c2 .getCenterX(), 2) + Math.pow(c1.getCenterY() - c2.getCenterY(), 2));
		rootLength /= 40;
		Organism o = new Organism(rootLength);
		if(((Circle)parents.get(0).get(2)).getFill() == Color.RED)
		{
			((Limb)o.getBody().get(0)).setFixed(true);
		}
		
		Coordinate rC = new Coordinate(((Circle)parents.get(0).get(1)).getCenterX(),((Circle)parents.get(0).get(1)).getCenterY());
		Coordinate rA = new Coordinate(((Circle)parents.get(0).get(2)).getCenterX(),((Circle)parents.get(0).get(2)).getCenterY());
		
		int offset = 10;
		Coordinate rB = new Coordinate(rC.getX() - offset,rC.getY());
	

		double ra = Math.sqrt( Math.pow(rC.getX() - rB.getX(),2) + Math.pow(rC.getY()-rB.getY(),2));
		double rb = Math.sqrt( Math.pow(rA.getX() - rC.getX(),2) + Math.pow(rA.getY()-rC.getY(),2));
		double rc = Math.sqrt( Math.pow(rA.getX() - rB.getX(),2) + Math.pow(rA.getY()-rB.getY(),2));
		
		float rootAngle = (float)(Math.PI - Math.acos((Math.pow(ra, 2) + Math.pow(rb, 2) - Math.pow(rc, 2))/(2*ra*rb)));
		rootAngle *= rA.getY() - rB.getY() <= 0?-1:1; 
		((Limb)o.getBody().get(0)).setAngle(rootAngle);
		
		for(int i=1;i<parents.size();i++)
		{
			Line cl = (Line)parents.get(i).get(0); 
			Circle p = (Circle)cP.get(cl);
			int j;
			for(j = 0; j < parents.size(); j++)
			{
				if(parents.get(j).contains(p)) 
				{
					break;
				}
			}
			Circle cc = (Circle)parents.get(i).get(1);   //CC #M. Lucci
			
			float size = (float)Math.sqrt(Math.pow(cl.getStartX() - cl.getEndX(),2) + Math.pow(cl.getStartY() - cl.getEndY(),2));
			size = (float)(size/(screenWidth/50));
			
			
			
			Coordinate B = new Coordinate(((Line)parents.get(j).get(0)).getStartX(),((Line)parents.get(j).get(0)).getStartY());
			Coordinate C = new Coordinate(((Circle)p).getCenterX(),((Circle)p).getCenterY());
			Coordinate A = new Coordinate(cc.getCenterX(),cc.getCenterY());
			
			
			float a =  (float)Math.sqrt(Math.pow(B.getX() - C.getX(),2) + Math.pow(B.getY() - C.getY(),2)); 
			float b =  (float)Math.sqrt(Math.pow(A.getX() - C.getX(),2) + Math.pow(A.getY() - C.getY(),2)); 
			float c =  (float)Math.sqrt(Math.pow(B.getX() - A.getX(),2) + Math.pow(B.getY() - A.getY(),2)); 
			
			int orient = ((B.getY() - C.getY())/(B.getX()-C.getX())) * (A.getX()*C.getX()) <= A.getY() - C.getY() ? 1 : -1;
			
			double angle = (Math.PI - Math.acos((Math.pow(a, 2) + Math.pow(b, 2) - Math.pow(c, 2))/(2*a*b))) * orient;
			
			Limb nL = new Limb(size,(Limb) o.getBody().get(j),((Circle)parents.get(i).getLast()).getFill() == Color.RED,(float)angle);
			
			o.addLimb(nL, j);
		}
		
		//System.out.println(o);
		
		return o;
	}

}
