package pr3_5.evolution_simulator.display;

public class OneGeneration extends Displayer 
{
	
	private int generationIndex;
	
	public OneGeneration(int gI)
	{
		this.generationIndex = gI;
	}
	
	public void Display() 
	{
		
	}

	public int GetGenerationIndex() 
	{
		return this.generationIndex;
	}

	public void SetGenerationIndex(int gI) 
	{
		this.generationIndex = gI;
	}
	
	
}
