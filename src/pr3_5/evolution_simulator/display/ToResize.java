package pr3_5.evolution_simulator.display;
/**
 * Set of value to determine the size of each elements according to the screen resolution.
 * @author Morel Jérémy
 *
 */
public enum ToResize {

	MAX_EDIT_BTN_WIDTH("100"),
	MAX_EDIT_BTN_HEIGHT("100"),
	SLIDER_WIDTH("150"),
	BOTTOM_BTN_WIDTH("150"),
	BOTTOM_BTN_HEIGHT("50"),
	TEXTFIELD_WIDTH("150"),
	SIMUPROGRESS_WIDTH("900"),
	TIMELEFT_WIDTH("150"),
	TIMELEFT_HEIGHT("25"),
	TEXTFIELD_HEIGHT("25");
	
	/**
	 * Size of element on layout
	 */
	private String size;
	
	/**
	 * ToResize constructor
	 * @param size
	 */
	private ToResize(String size)
	{
		this.size = size;
	}
	
	/**
	 * Determine the size of elements according to the maximal screen 
	 * resolution (1920x1080) and the current resolution
	 * @param toSizing : A value from the enum ToResize.
	 * @param screenPropertie : current screen propertie (it depends if we need screen height or screen width)
	 * @param maxScreenPropertie : max screen propertie (it depends if we nedd screen height or screen)
	 * @return the size according to the resolution
	 */
	public static double resizing(String toSizing, double screenPropertie, double maxScreenPropertie)
	{
		return Double.valueOf(toSizing)*(screenPropertie/maxScreenPropertie);
	}
	/**
	 * @return the size of elements for a 1920x1080 resolution.
	 */
	public String toString()
	{
		return this.size;
	}
}

