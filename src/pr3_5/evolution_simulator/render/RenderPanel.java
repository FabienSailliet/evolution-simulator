package pr3_5.evolution_simulator.render;

import java.awt.Color;
import java.awt.Font;
import java.awt.Graphics;
import java.awt.Toolkit;
import java.util.concurrent.TimeUnit;

import javax.swing.JLabel;
import javax.swing.JPanel;

import org.jbox2d.common.Vec2;
import org.jbox2d.dynamics.Body;

import pr3_5.evolution_simulator.simulator.OrganismSimulation;
import pr3_5.evolution_simulator.simulator.bodies.DrawableBody;

public class RenderPanel extends JPanel {
	/** Avoid serialization warning */
	private static final long serialVersionUID = 1L;
	
	/** Duration of each simulation step */
	private static final float stepTime = (1.0f/60);
	
	private static final float CAMERA_HEIGHT = 3;
	
	/** Label to display traveled distance */
	private final JLabel distLabel;
	
	private OrganismSimulation simulation;
	/** Display scale in pixel/m */
	private int scale;
	/** Start x coordinate */
	private float xDelta;
	/** To measure elapsed time between two frames */
	private long prevTime;
	
	public RenderPanel(OrganismSimulation simu, int scale) {
		super();
		
		// Distance label
		this.distLabel = new JLabel();
		this.distLabel.setFont(new Font("Arial", Font.PLAIN, 40));
		
		this.add(this.distLabel);
		
		this.simulation = simu;
		this.scale = scale;
		
		simu.restart();
		
		this.xDelta = this.simulation.getCenterOn().getWorldCenter().x;
	}

	public void paintComponent(Graphics g) {
		
		// Simulate
		this.simulation.step(RenderPanel.stepTime);
		
		// Clear screen
		g.setColor(Color.WHITE);
		g.fillRect(0, 0, this.getWidth(), this.getHeight());
		
		// Get position where center camera
		Vec2 centerOn = this.simulation.getCenterOn().getWorldCenter();
		
		this.distLabel.setText("Distance : " + Math.round(centerOn.x - this.xDelta) + "m");
		
		// Paint bodies on screen
		this.paintWorld(g);
		
		// Calculate delta time
		long time = System.nanoTime();
		double dt = (double) (time - this.prevTime) / TimeUnit.MILLISECONDS.toNanos(1);
		
		// Pause
		try {
			TimeUnit.MILLISECONDS.sleep((int) (RenderPanel.stepTime*1000 - dt));
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
		
		this.prevTime = System.nanoTime();
		
		this.repaint();
		// Make smooth animation
		Toolkit.getDefaultToolkit().sync();
	}
	
	/**
	 * Display world in a JPanel
	 * 
	 * @param g Graphics object of a JPanel
	 */
	private void paintWorld(Graphics g) {
		Vec2 centerOn;
		
		// If the floor is horizontal, don't move camera on y
		if (this.simulation.getFloorAngle() == 0) {
			centerOn = new Vec2(this.simulation.getCenterOn().getWorldCenter().x, 
					this.simulation.getGroundHeight()-RenderPanel.CAMERA_HEIGHT);
		}
		else {
			centerOn = this.simulation.getCenterOn().getWorldCenter();
		}
		
		
		for (Body body=this.simulation.getWorld().getBodyList() ; body != null ; body=body.getNext()) {
			try {
				((DrawableBody) body).draw(g, this.scale, centerOn, this.getWidth(), this.getHeight());
			}
			// If body is not drawable
			catch (ClassCastException e) {}
		}
		
		
	}
}
