package pr3_5.evolution_simulator.render;

import javax.swing.JFrame;

import pr3_5.evolution_simulator.simulator.OrganismSimulation;

public class RenderWindow extends JFrame {
	/** Avoid serialization warning */
	private static final long serialVersionUID = 1L;
	
	public RenderWindow(OrganismSimulation simu, int scale, String title) {
		super();
		
		this.setTitle(title);
		this.setSize(800, 600);
		this.setLocationRelativeTo(null);
		
		this.setContentPane(new RenderPanel(simu, scale));
		
		this.setVisible(true);
	}
}
