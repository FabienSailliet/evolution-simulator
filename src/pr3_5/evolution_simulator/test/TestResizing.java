package pr3_5.evolution_simulator.test;

import junit.framework.Test;
import junit.framework.TestCase;
import junit.framework.TestSuite;

public class TestResizing extends TestCase {

	public TestResizing(String name)
	{
		super(name);
	}
	
	public void stringOk()
	{
		double maxScreenWidth = 1920;
		double screenWidth = 800;
		String maxSize = "250";
		double res = Double.valueOf(maxSize)*(screenWidth/maxScreenWidth);
		assertEquals(168.75, res);	
	}
	
	public void valeurMax()
	{
		double maxScreenWidth = 1920;
		double screenWidth = 1920;
		String maxSize = "250";
		double res = Double.valueOf(maxSize)*(screenWidth/maxScreenWidth);
		assertEquals(450.0,res);
	}
	
	public void stringNotOk()
	{
		String maxSize = "toto";
		for(char c:maxSize.toCharArray())
		{
			if(c != 1 || c != 2 || c != 3 || c!=4 || c!=5 || c!=6 || c!=7 || c!=8 || c!=9)
			{
				assertNotSame("450.0",maxSize);
				break;
			}
		}
	}
	
	public static Test suite()
	{
		TestSuite suite = new TestSuite();
		suite.addTest(new TestToSpacing("stringOk"));
		suite.addTest(new TestToSpacing("valeurMax"));
		suite.addTest(new TestToSpacing("stringNotOk"));
		return suite;
	}
}
