package pr3_5.evolution_simulator.simulator.bodies;
import java.awt.Color;

import org.jbox2d.collision.shapes.PolygonShape;
import org.jbox2d.common.Vec2;
import org.jbox2d.dynamics.BodyDef;
import org.jbox2d.dynamics.FixtureDef;
import org.jbox2d.dynamics.World;
import org.jbox2d.dynamics.joints.RevoluteJoint;
import org.jbox2d.dynamics.joints.RevoluteJointDef;
import org.jbox2d.dynamics.joints.WeldJointDef;

import pr3_5.evolution_simulator.simulator.Action;

/**
 * Limb, chained with other ones
 * 
 * @author fabien
 *
 */
public class Branch extends DrawableBody {
	/** Density in kg/m^2 */
	private final static float DENSITY = 1f;
	/** Restitution when contact with a surface */
	private final static float RESTITUTION = 0;
	/** Friction with surfaces */
	private final static float FRICTION = 1;
	
	/** Max torque of the articulation */
	private final static float MAX_MOTOR_TORQUE = 50;
	
	/** Allow limb colliding together or not */
	private final static int GROUP_INDEX = 1;
	
	/** Width of a branch */
	private static final float WIDTH = 0.2f;
	
	private RevoluteJoint articulation;
	/** Actions array */
	private Action[] actions;
	
	/** Length of the limb */
	private float length;
	
	/** Has a fixed child joint */
	private boolean hasWeldedArt;
	
	/**
	 * Create new limb
	 * 
	 * @param bd body definition
	 * @param world
	 * @param length length of the limb
	 * @param hasWeldedArt has a fixed child joint
	 * @param actions 
	 * @param color
	 */
	public Branch(BodyDef bd, World world, float length, boolean hasWeldedArt, Action[] actions, Color color) {
		super(bd, world, color);
		
		this.length = length;
		this.hasWeldedArt = hasWeldedArt;
		this.actions = actions;
		
		PolygonShape shape = new PolygonShape();
		shape.setAsBox(length/2, Branch.WIDTH/2, new Vec2(length/2, 0), 0);
		
		FixtureDef fd = new FixtureDef();
		fd.shape = shape;
		fd.density = Branch.DENSITY;
		fd.friction = Branch.FRICTION;
		fd.restitution = Branch.RESTITUTION;
		fd.filter.groupIndex = Branch.GROUP_INDEX;
		
		this.createFixture(fd);
	}
	
	/**
	 * Append a child at the end of the limb
	 * 
	 * @param length length of the limb
	 * @param angle angle between new limb and parent
	 * @param hasWeldedArt has a fixed child articulation
	 * @param actions
	 * @param color
	 * @return the new limb
	 */
	public Branch addChild(float length, float angle, boolean hasWeldedArt, Action[] actions, Color color) {
		BodyDef bd = new BodyDef();
		bd.position.set(this.getWorldPoint(new Vec2(this.length, 0)));
		bd.type = this.m_type;
		bd.angle = angle;
		
		Branch childBranch = new Branch(bd, this.getWorld(), length, hasWeldedArt, actions, color);
		
		if (this.hasWeldedArt) {
			childBranch.addWeldJoint(this);
		}
		else {
			childBranch.addRevoluteJoin(this);
		}
		
		return childBranch;
	}
	
	private void addRevoluteJoin(Branch parentBranch) {
		RevoluteJointDef jd = new RevoluteJointDef();
		jd.initialize(parentBranch, this, this.getWorldPoint(new Vec2(0, 0)));
		jd.enableMotor = true;
		jd.maxMotorTorque = Branch.MAX_MOTOR_TORQUE;
		
		this.articulation = (RevoluteJoint) this.m_world.createJoint(jd);
	}
	
	private void addWeldJoint(Branch parentBranch) {
		WeldJointDef jd = new WeldJointDef();
		jd.initialize(parentBranch, this, this.getWorldPoint(new Vec2(0, 0)));
		
		this.m_world.createJoint(jd);
	}
	
	/**
	 * Perform a specific action
	 * 
	 * @param actionNumber
	 */
	public void doAction(int actionNumber) {
		try {
			Action action = this.actions[actionNumber];
			
			this.articulation.setMotorSpeed((float) action.getSpeed());
		}
		catch (NullPointerException e) {}
	}
	
}
