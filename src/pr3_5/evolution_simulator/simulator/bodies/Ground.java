package pr3_5.evolution_simulator.simulator.bodies;
import java.awt.Color;
import java.awt.GradientPaint;
import java.awt.Graphics;
import java.awt.Graphics2D;

import org.jbox2d.collision.shapes.PolygonShape;
import org.jbox2d.common.Vec2;
import org.jbox2d.dynamics.BodyDef;
import org.jbox2d.dynamics.Fixture;
import org.jbox2d.dynamics.FixtureDef;
import org.jbox2d.dynamics.World;

import pr3_5.evolution_simulator.display.Utilities;

/**
 * Rectangle
 * @author fabien
 *
 */
public class Ground extends DrawableBody {
	
	/**
	 * Construct a new wall
	 * @param bd
	 * @param world
	 * @param width
	 * @param height
	 * @param density
	 * @param friction
	 * @param restitution
	 * @param color
	 */
	public Ground(BodyDef bd, World world, float width, float height, float density, float friction, float restitution, Color color) {
		super(bd, world, color);
		
		PolygonShape shape = new PolygonShape();
		shape.setAsBox(width/2, height/2);
		
		FixtureDef fd = new FixtureDef();
		fd.shape = shape;
		fd.density = density;
		fd.friction = friction;
		fd.restitution = restitution;
		
		this.createFixture(fd);
	}

	@Override
	public void draw(Graphics g, int scale, Vec2 camPos, int screenWidth, int screenHeight) {
		Graphics2D g2 = (Graphics2D) g;

		int xOrig = Utilities.toPixCoord(0, scale, camPos.x, screenWidth);
		int yOrig = Utilities.toPixCoord(0, scale, camPos.y, screenHeight);
		
		GradientPaint gp = new GradientPaint(xOrig, yOrig, this.color, xOrig+100, yOrig+10, this.color.darker(), true);                

	    g2.setPaint(gp);
		
	    // A grounds has one fixture
		Fixture fix = this.getFixtureList();
		PolygonShape polShape = (PolygonShape) fix.getShape();
		
		// Seems returns an array half-empty
		Vec2[] vertices = polShape.getVertices();
		int[] xPoints = new int[vertices.length/2];
		int[] yPoints = new int[vertices.length/2];
		
		for (int i=0 ; i<vertices.length/2 ; i++) {
			Vec2 worldP = this.getWorldPoint(vertices[i]);
			xPoints[i] = Utilities.toPixCoord(worldP.x, scale, camPos.x, screenWidth);
			yPoints[i] = Utilities.toPixCoord(worldP.y, scale, camPos.y, screenHeight);
		}
		
		g2.fillPolygon(xPoints, yPoints, vertices.length/2);
		
	}
	
	
	
}
