package pr3_5.evolution_simulator.simulator.bodies;
import java.awt.Color;
import java.awt.Graphics;

import org.jbox2d.collision.shapes.CircleShape;
import org.jbox2d.collision.shapes.PolygonShape;
import org.jbox2d.collision.shapes.Shape;
import org.jbox2d.common.Vec2;
import org.jbox2d.dynamics.Body;
import org.jbox2d.dynamics.BodyDef;
import org.jbox2d.dynamics.Fixture;
import org.jbox2d.dynamics.World;

import pr3_5.evolution_simulator.display.Utilities;

/**
 * JBox2D body that can be displayed in a JPanel
 * 
 * @author fabien
 *
 */
public abstract class DrawableBody extends Body {
	/** Display color */
	protected Color color;
	
	/**
	 * Construct a new DrawableBody
	 * 
	 * @param bd body definition
	 * @param world
	 * @param color display color of the body
	 */
	public DrawableBody(BodyDef bd, World world, Color color) {
		super(bd, world);
		
		world.addBody(this);
		
		this.color = color;
	}
	
	/**
	 * Draw an object in a JFrame
	 * @param g graphics object to draw
	 * @param scale size in pixel of a simulation meter
	 * @param camPos point where center camera
	 * @param screenWidth
	 * @param screenHeight
	 */
	public void draw(Graphics g, int scale, Vec2 camPos, int screenWidth, int screenHeight) {
		g.setColor(this.color);
		
		Fixture fix = this.getFixtureList();
		while (fix != null) {
			Shape shape = fix.getShape();
			
			switch (shape.getType()) {
			case POLYGON:
				
				PolygonShape polShape = (PolygonShape) shape;
				
				// Seems returns an array half-empty
				Vec2[] vertices = polShape.getVertices();
				int[] xPoints = new int[vertices.length/2];
				int[] yPoints = new int[vertices.length/2];
				
				for (int i=0 ; i<vertices.length/2 ; i++) {
					Vec2 worldP = this.getWorldPoint(vertices[i]);
					xPoints[i] = Utilities.toPixCoord(worldP.x, scale, camPos.x, screenWidth);
					yPoints[i] = Utilities.toPixCoord(worldP.y, scale, camPos.y, screenHeight);
				}
				
				g.fillPolygon(xPoints, yPoints, vertices.length/2);
				
				break;
			case CHAIN:
				
				break;
			case CIRCLE:
				
				CircleShape cirShape = (CircleShape) shape;
				
				Vec2 pos = this.getPosition();
				float radius = cirShape.getRadius();

				int xCircle = Utilities.toPixCoord(pos.x, scale, camPos.x, screenWidth);
				int yCircle = Utilities.toPixCoord(pos.y, scale, camPos.y, screenHeight);
				int pixRadius = Utilities.toPixSize(radius, scale);
				
				g.drawOval(xCircle-pixRadius, yCircle-pixRadius, pixRadius*2, pixRadius*2);

				int x2 = Utilities.toPixCoord(pos.x + Math.cos(this.getAngle()) * radius, scale, camPos.x, screenWidth);
				int y2 = Utilities.toPixCoord(pos.y + Math.sin(this.getAngle()) * radius, scale, camPos.y, screenHeight);
				
				g.drawLine(xCircle, yCircle, x2, y2);
				
				break;
			case EDGE:
				
				break;
			default:
				
				break;
			}
			
			fix = fix.getNext();
		}
	}
}
