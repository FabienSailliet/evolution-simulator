package pr3_5.evolution_simulator.simulator;
import java.util.LinkedList;

/**
 * this class provide the behavior of one generation of organism.
 * @author GELLENONCOURT Michael
 *
 */
public class Simulation 
{
	private final Simulator simulator;
	
	/**
	 * the generation of the simulation.
	 */
	private LinkedList<Organism> generation;
	
	/**
	 * the size of the generation. 
	 */
	private int generationSize;
	
	public Simulation(Simulator simulator, int generationSize, float mutationChance,Organism b) 
	{
		this.simulator = simulator;
		this.generationSize = generationSize;
		this.generation = new LinkedList<Organism>();
		this.generation.add(b);
		while(this.generation.size() < generationSize)
		{
			this.generation.add(b.reproduce());
		}
	}
	/**
	 * allow us to run the simulation, and simulate the life of each organism according to their genom.
	 */
	public Organism Simulate()
	{
		for(int i=0;i < this.generationSize;i++)
		{
			OrganismSimulation oS = new OrganismSimulation(this.generation.get(i),this.simulator);
			oS.step(Simulator.TIME);
			this.generation.get(i).evaluate(oS.getAverageSpeed());
			this.generation.get(i).setOrganismSimu(oS);
		}
		
//		System.out.println(this.getBestOrganism());
//		System.out.println(this.getBestOrganism().mutate(this.mutationChance));
		return this.getBestOrganism().reproduce();
	}
	/**
	 * allow us to sort all the organism in order to reproduce the two winner later on.
	 */
	public Organism getBestOrganism()
	{
//		LinkedList<Organism> nL = new LinkedList<Organism>();
//		
//		for(int i=0;i<this.generationSize;i++)
//		{
//			float max = -1000000;
//			int maxIndex = -1;
//			for(int j=0;j<this.generationSize-i;j++)
//			{
//				float score = this.generation.get(j).getScore();
//				if(max < score)
//				{
//					max = score;
//					maxIndex = j;
//				}
//			}
//			
//			if(maxIndex != -1)
//			{
//				nL.add(this.generation.get(maxIndex));
//				this.generation.remove(maxIndex);
//			}
//		}
//		this.generation = nL;
		
		Organism ret = this.generation.get(0);
		
		for(int i=1;i < this.generation.size();i++)
		{
			ret = ret.getScore() < this.generation.get(i).getScore() ? this.generation.get(i):ret;
		}
		return ret;
	}
}
