package pr3_5.evolution_simulator.simulator;

import java.util.LinkedList;

/**
 * this is an implementation of BodyParts providing the behavior of a limb.
 * @author GELLENONCOURT Michael
 *
 */
public class Limb extends BodyPart
{
	/**
	 * the length of the limb.
	 */
	private float length;
	
	private boolean fixed;
	
	private float angle;
	private Limb p;
	public static final int NBA = 30;
	private LinkedList<Limb> children;
	private Action[] acts;
	
	/**
	 * this constructor provide the creation of a limb with is length
	 * @param l
	 */
	public Limb(float l,Limb p,boolean f,float a)
	{
		this.p = p;
		this.fixed = f;
		this.angle = a;
		this.length = l;
		this.children = new LinkedList<>();
		
		
		if(this.p != null && this.p.fixed == false)
		{
			this.acts = new Action[Limb.NBA];
			for(int i=0;i<Limb.NBA;i++)
			{
				this.acts[i] = new Action();
			}
		}
		else
		{
			this.acts = null;
		}
	}
	
	public void setFixed(boolean fixed) {
		this.fixed = fixed;
	}

	public boolean isFixed() 
	{
		return fixed;
	}

	public Limb(float length, Limb p, LinkedList<Limb> children, Action[] acts,float a,boolean f) 
	{
		this.length = length;
		this.p = p;
		this.fixed = f;
		this.children = children;
		this.acts = acts;
		this.angle = a;
	}

	public void addChild(Limb l)
	{
		this.children.add(l);
	}
	/**
	 * @return the length of the limb
	 */
	public float getLength() 
	{
		return this.length;
	}
	
	public void setRoot(Limb l)
	{
		this.p = l;
	}
	
	public Action getAction(int index)
	{
		if(this.acts != null && index < Limb.NBA)
		{
			return this.acts[index];
		}
		return null;
	}
	
	/**
	 * Return actions
	 * @return actions array
	 */
	public Action[] getActions() {
		return this.acts;
	}

	public BodyPart reproduce() 
	{
		Action[] newA = this.acts == null?null:new Action[Limb.NBA];
		if(this.acts != null)
		{
			double moy;
			for(int i=0;i<Limb.NBA;i++)
			{
				moy = this.acts[i].getSpeed() + (Math.random()*Action.MAX_SPEED * 0.1 * (Math.random() < 0.5?1:-1));	
				moy = moy > Action.MAX_SPEED?Action.MAX_SPEED:moy < -Action.MAX_SPEED?-Action.MAX_SPEED:moy;
				newA[i] = new Action((float)moy);
			}
		}
		
		return new Limb(this.length,this.p,new LinkedList<Limb>(),newA,this.angle,this.fixed);
	}

	public void setAngle(float angle) {
		this.angle = angle;
	}

	public Limb getP() 
	{
		return this.p;
	}
	
	public void setP(Limb p) {
		this.p = p;
	}

	public void setLength(float length) {
		this.length = length;
	}

	/**
	 * Get angle
	 * 
	 * @return angle between a limb and his parent
	 */
	public float getParentAngle() {
		return this.angle;
	}
	
	/**
	 * Get child limbs
	 * 
	 * @return child limbs
	 */
	public LinkedList<Limb> getChildLimbs() {
		return this.children;
	}
	
	
}
