package pr3_5.evolution_simulator.simulator;

import java.awt.Color;

import org.jbox2d.common.Vec2;
import org.jbox2d.dynamics.Body;
import org.jbox2d.dynamics.BodyDef;
import org.jbox2d.dynamics.BodyType;
import org.jbox2d.dynamics.World;

import pr3_5.evolution_simulator.simulator.bodies.Branch;
import pr3_5.evolution_simulator.simulator.bodies.DrawableBody;
import pr3_5.evolution_simulator.simulator.bodies.Ground;

/**
 * Simulate a single organism in an empty world
 * @author fabien
 *
 */
public class OrganismSimulation {
	private static final float GROUND_WIDTH = 10;
	private static final float GROUND_LENGTH = 100;
	
	/** Maximum allowed time for a simulation step */
	private static final float MAX_STEP_TIME = 0.1f;
	private static final int VELOCITY_ITERATIONS = 6;
	private static final int POSITION_ITERATIONS = 3;
	
	private static final Color COLOR = Color.PINK;
	private final Simulator simulator;
	
	/** Organism to simulate */
	private Organism organism;
	/** JBox2D simulated world */
	private World world;
	
	/** maximum distance traveled by the organism */
	private float maxDistance;
	/** Body to follow during rendering */
	private DrawableBody centerOn;
	/** Elapsed time in the simulation, in seconds */
	private float time;
	
	/**
	 * Construct a new OrganismSimulation
	 * 
	 * @param organism
	 * @param simulator
	 */
	public OrganismSimulation(Organism organism, Simulator simulator) {
		this.simulator = simulator;
		this.organism = organism;
		
		this.restart();
	}
	
	/**
	 * Reset the world by create a new one, and recreate the organism at the origin
	 */
	public void restart() {
		this.maxDistance = 0;
		this.time = 0;
		
		// Create world with gravity
		this.createWorld(simulator.getGravity());
		
		// Ground
		this.centerOn = this.addGround(-simulator.getFloorAngle());
		
		// Organism
		this.addOrganism(this.organism);
	}

	/**
	 * Create a new world
	 * 
	 * @param gravity
	 */
	private void createWorld(float gravity) {
		this.world = new World(new Vec2(0, gravity));
	}
	
	/**
	 * Add a ground
	 * 
	 * @param angle rise angle
	 */
	private Ground addGround(float angle) {
		BodyDef bd = new BodyDef();
		
		bd.angle = angle;
		bd.position.set(0, 0);
		bd.type = BodyType.STATIC;
		return new Ground(bd, this.world, OrganismSimulation.GROUND_LENGTH, OrganismSimulation.GROUND_WIDTH, 1, 0.3f, 0f, Color.GREEN);
	}
	
	/**
	 * Add an organism at the origin of the world
	 * 
	 * @param organism organism to add
	 */
	private void addOrganism(Organism organism) {
		Limb limb = (Limb) organism.getBody().getFirst();
		
		if (limb != null) {
			BodyDef bd = new BodyDef();
			bd.type = BodyType.DYNAMIC;
			bd.position.set(0, -organism.getRootHeight() - OrganismSimulation.GROUND_WIDTH/2);
			bd.angle = limb.getParentAngle();
			
			// Add root limb in world
			Branch branch = new Branch(bd, this.world, limb.getLength(),limb.isFixed(), limb.getActions(), OrganismSimulation.COLOR);
			this.addChildLimbs(limb, branch);
			
			this.centerOn = branch;
		}
	}
	
	/**
	 * Append a branch on an existing one
	 * 
	 * @param limb limb to import in the world
	 * @param branch existing branch in the world
	 */
	private void addChildLimbs(Limb limb, Branch branch) {
		for (Limb childLimb: limb.getChildLimbs()) {
			Branch childBranch = branch.addChild(childLimb.getLength(), childLimb.getParentAngle(), childLimb.isFixed(), childLimb.getActions(), OrganismSimulation.COLOR);
			this.addChildLimbs(childLimb, childBranch);
		}
	}
	
	/**
	 * Simulate. Take care about use a large time, or a constant one if it's smaller than MAX_STEP_TIME
	 * 
	 * @param time amount of time to simulate, in seconds
	 */
	public void step(float time) {

		// If time too long, cut it into smaller ones
		float stepTime = (time > OrganismSimulation.MAX_STEP_TIME)? OrganismSimulation.MAX_STEP_TIME : time;
		
		for (; time >= stepTime ; time-=stepTime) {
			this.time += stepTime;
			
			// Determinate which action trigger
			int actionNumber = (int) ((this.time / Simulator.ACTION_TIME) % Limb.NBA);
			
			for (Body body = this.world.getBodyList() ; body.getNext() != null ; body = body.getNext()) {
				if (body instanceof Branch) {
					Branch branch = (Branch) body;
					branch.doAction(actionNumber);
					
					float dist = branch.getPosition().x;
					this.maxDistance = (dist > this.maxDistance)? dist : this.maxDistance;
				}
			}
			
			this.world.step(stepTime, OrganismSimulation.VELOCITY_ITERATIONS, OrganismSimulation.POSITION_ITERATIONS);
		}
		
	}
	
	/**
	 * Get maximum distance traveled by the organism
	 * 
	 * @return maximum distance traveled
	 */
	public float getMaxDistance() {
		return this.maxDistance;
	}
	
	/**
	 * Get average speed of the organism on the ground
	 * 
	 * @return average speed on the ground
	 */
	public float getAverageSpeed() {
		return this.centerOn.getPosition().x / this.time;
	}
	
	/**
	 * Get JBox2D world
	 * 
	 * @return JBox2D world
	 */
	public World getWorld() {
		return this.world;
	}
	
	/**
	 * Get the centered body
	 * 
	 * @return targeted body
	 */
	public DrawableBody getCenterOn() {
		return this.centerOn;
	}
	
	/**
	 * Get the ground height
	 * 
	 * @return the y coordinate of the surface
	 */
	public float getGroundHeight() {
		return -OrganismSimulation.GROUND_WIDTH/2;
	}
	
	/**
	 * Get the floor angle
	 * 
	 * @return the floor angle
	 */
	public float getFloorAngle() {
		return this.simulator.getFloorAngle();
	}
}
