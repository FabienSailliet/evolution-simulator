package pr3_5.evolution_simulator.simulator;
import java.util.LinkedList;

import javafx.scene.control.ProgressBar;
import javafx.scene.control.TextField;

/**
 * this class provide the behavior of the simulator. this is the main class.
 * @author GELLENONCOURT Michael
 *
 */
public class Simulator 
{
	public static final float TIME = 20;
	public static final float ACTION_TIME = 0.1f;
	
	/** Simulated world gravity */
	private float gravity;
	
	/** this is the amount of generation to be simulated. */
	private int nbGeneration;
	
	/** this is the amount of organism in one generation. */
	private int generationSize;
	
	/** ground rise angle */
	private int floorAngle;
	
	/** this is the mutation chance. */
	private float mutationChance;
	
	/** this is the base Organism of the simulator. */
	 private Organism baseOrganism;
	private Organism newGeneBaOrga;
	private LinkedList<Simulation> allSimu;
	
	
	
	
	
	
	public Simulator(float gravity, int nbGeneration, int generationSize, int floorAngle, float mutationChance,Organism baseOrganism) 
	{
		this.gravity = gravity;
		this.nbGeneration = nbGeneration;
		this.generationSize = generationSize;
		this.floorAngle = floorAngle;
		this.mutationChance = mutationChance;
		this.baseOrganism = baseOrganism;
		this.newGeneBaOrga = this.baseOrganism;
		this.allSimu = new LinkedList<Simulation>();
	}
	
	public OrganismSimulation getOrgaSimu(int index)
	{
		return this.allSimu.get(index).getBestOrganism().getOrganismSimu();
	}
	
	public Simulation newSimulation()
	{
		//TODO change last param
		
		Simulation newS = new Simulation(this,this.generationSize,this.mutationChance,this.newGeneBaOrga);
		
		this.newGeneBaOrga = newS.Simulate();
		
		return newS;
		//this.newGeneBaOrga = newS.Simulate();
	}
	public Organism getOrga()
	{
		return this.newGeneBaOrga;
	}
	public void run(TextField TimeLeft , ProgressBar b)
	{
		
		Long startTime = System.currentTimeMillis();
		for(int i=0;i < this.nbGeneration;i++)
		{
			b.setProgress((float)((float)i/(float)this.nbGeneration));
			Long newTime = System.currentTimeMillis();
			
			Long timeSpent = newTime - startTime;
			Long TotalTime = (timeSpent*this.nbGeneration)/(i+1);
			Long remainTime = TotalTime - timeSpent;
			
			Long nbH = remainTime / 3600000;
			remainTime %= 3600000;
			Long nbM = remainTime / 60000;
			remainTime %= 60000;
			Long nbS = remainTime / 1000;
			remainTime %=1000;
			nbS += remainTime > 0 ? 1:0;
			//System.out.println(formatter.format(date));
			TimeLeft.setText(String.format("%d h %d min %d sec left",nbH,nbM,nbS));
//			st.setScene(sc); 
//			st.show(); 
			Simulation s = this.newSimulation();
			if(i == 0 || i == this.nbGeneration-1)
			{
				this.allSimu.add(s);
				
			}
			
		}
	}
	/**
	 * @return the gravity of the world
	 */
	public float getGravity() 
	{
		return this.gravity;
	}
	
	/**
	 * @return the amount of generation to be simulated
	 */
	public int getNbGeneration() 
	{
		return this.nbGeneration;
	}
	
	/**
	 * @return the amount of organism per generation 
	 */
	public int getGenerationSize() 
	{
		return this.generationSize;
	}
	
	/**
	 * @return the floor angle of the world.
	 */
	public int getFloorAngle() 
	{
		return this.floorAngle;
	}
	
	@Override
	public String toString() {
		return "Simulator [gravity=" + gravity + ", nbGeneration=" + nbGeneration + ", generationSize=" + generationSize
				+ ", floorAngle=" + floorAngle + ", mutationChance=" + mutationChance + ", baseOrganism=" + baseOrganism
				+ ", allSimu=" + allSimu + "]";
	}

	/**
	 * @return the mutation chance.
	 */
	public float getMutationChance() 
	{
		return this.mutationChance;
	}

	public Organism getBaseOrganism() 
	{
		return baseOrganism;
	}
	
}
