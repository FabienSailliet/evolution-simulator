package pr3_5.evolution_simulator.simulator;
/**
 * abstract class for the body parts of the organism.
 * @author GELLENONCOURT Michael
 *
 */
public abstract class BodyPart 
{
	public BodyPart()
	{
		
	}

	public abstract BodyPart reproduce();
	public abstract void setLength(float length);
}
