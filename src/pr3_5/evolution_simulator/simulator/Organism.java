package pr3_5.evolution_simulator.simulator;
import java.util.LinkedList;
/**
 * this class provide the behavior of an organism.
 * @author GELLENONCOURT Michael
 *
 */
public class Organism 
{
	
	/**
	 * the list of the body parts of the organism.
	 */
	private LinkedList<BodyPart> Body; 
	private float score;
	private OrganismSimulation organismSimu;
	/**
	 * this constructor provide the creation of a base organism composed of a limb and two articulations. 
	 */
	public Organism()
	{
		this.score = 0;
		this.Body = new LinkedList<BodyPart>();
		
		Limb l = new Limb(5,null,false,0);
	
		this.Body.add(l);
		
	}
	public Organism(float rootLength)
	{
		this.score = 0;
		this.Body = new LinkedList<BodyPart>();
		
		Limb l = new Limb(rootLength,null,false,0);
	
		this.Body.add(l);
		
	}
	
	public float getScore() {
		return score;
	}
	public OrganismSimulation getOrganismSimu() {
		return organismSimu;
	}

	public void setOrganismSimu(OrganismSimulation organismSimu) {
		this.organismSimu = organismSimu;
	}

	/**
	 * this allow the organism to reproduce himself.
	 * @return the Genom resulting of the reproduction 
	 */
	public Organism reproduce() 
	{
		Organism newO = new Organism(((Limb)this.Body.get(0)).getLength());
		for(int i=1;i<this.Body.size();i++)
		{
			//System.out.println("Reproduce = " + ((Limb)this.Body.get(i)).getP());
			newO.addLimb((Limb)this.Body.get(i).reproduce(), this.Body.indexOf(((Limb)this.Body.get(i)).getP()));
		}
		return newO;
	}
	
	/**
	 * allow us to evaluate the performance of an organism in a generation. 
	 * @param f 
	 */
	public void evaluate(float f)
	{
		this.score = f;
	}
	
	public void addLimb(float size,int p,boolean f,float a)  
	{	
		Limb l2 = (Limb)this.Body.get(p);
		Limb l = new Limb(size,l2,f,a);
		l2.addChild(l);
		this.Body.add(l);
	}
	public void addLimb(Limb l,int p)  
	{	
//		System.out.println("        " + p);
		Limb l2 = (Limb)this.Body.get(p);
		l2.addChild(l);
		l.setP(l2);
		this.Body.add(l);
	}
	public float getLimbLength(int index)
	{
		return ((Limb)this.Body.get(index)).getLength();
	}
	
	/**
	 * @return the body of the organism
	 */
	public LinkedList<BodyPart> getBody()
	{
		return this.Body;
	}
	
	/**
	 * Calculate the height of the root point
	 * 
	 * @return height between root extremity of the root limb and ground
	 */
	public float getRootHeight() {
		// TODO getRootHeight method in Organism
		return 5;
	}
	
	
	
}
