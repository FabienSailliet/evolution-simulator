package pr3_5.evolution_simulator.simulator;

/**
 * Action Class.
 * This class provide Action behavior
 * @author GELLENONCOURT Michael
 *
 */
public class Action  
{
	/**
	 * MAX_SPEED constant.
	 * This is the maximum speed for the action
	 */
	public static final int MAX_SPEED = 10;
	
	/**
	 * Speed attribute.
	 * This is the movement's speed
	 */
	private double speed;
	
	
	/**
	 * Create a random action
	 */
	public Action() 
	{
		this.speed = Math.random()*Action.MAX_SPEED * ((Math.random()>0.5)? -1 : 1);
	}
	public Action(float s) 
	{
		this.speed = s;
	}

	/**
	 * give us the speed of the action.
	 * @return the speed of the action
	 */
	public double getSpeed() 
	{
		return this.speed;
	}
	
	/**
	 * set the speed of the action.
	 */
	public void setSpeed(double s) 
	{
		this.speed = s;
	}
	
}
